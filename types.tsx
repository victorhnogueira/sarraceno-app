/**
 * Learn more about using TypeScript with React Navigation:
 * https://reactnavigation.org/docs/typescript/
 */

export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  TabPratos: undefined;
  TabShoppingCart: undefined;
};

export type TabPratosParamList = {
  TabPratosScreen: undefined;
};

export type TabShoppingCartParamList = {
  TabShoppingCartScreen: undefined;
};
