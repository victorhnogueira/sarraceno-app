import * as React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  TouchableHighlight,
  Modal,
} from "react-native";
import { useIsFocused } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import NumberFormat from "react-number-format";
import LottieView from "lottie-react-native";
// @ts-ignore
import RadioButtonRN from "radio-buttons-react-native";

import { Text, View } from "../components/Themed";
import {
  getCart,
  subProductQtd,
  addProductQtd,
  emptyCart,
  getTotal,
} from "../services/cart";
import { useState, useEffect, Fragment } from "react";
import { ScrollView, TextInput } from "react-native-gesture-handler";

interface TabShoppingCartProps {
  navigation: any;
}

export default function TabShoppingCartScreen({
  navigation,
}: TabShoppingCartProps) {
  const [carrinho, setCarrinho] = useState<any>([]);
  const [total, setTotal] = useState(0);
  const [deliveryFee, setDeliveryFee] = useState(4.99);
  const [modalFinishOrderVisible, setModalFinishOrderVisible] = useState(false);
  const [orderAddress, setOrderAddress] = useState("");
  const isFocused = useIsFocused();

  async function loadCart() {
    const cart = (await getCart()) ?? "[]";
    setCarrinho(JSON.parse(cart));
  }

  async function subQtd(hash: string) {
    await subProductQtd(hash);
    await loadCart();
  }

  async function addQtd(hash: string) {
    await addProductQtd(hash);
    await loadCart();
  }

  async function esvaziarCart() {
    await emptyCart();
    await loadCart();
  }

  useEffect(() => {
    loadCart();
  }, [isFocused]);

  useEffect(() => {
    async function loadTotal() {
      const t = await getTotal();
      setTotal(t);
    }

    loadTotal();
  }, [carrinho]);

  const paymentMethod = [
    {
      label: "Dinheiro",
      value: 1,
    },
    {
      label: "Cartão de crédito",
      value: 2,
    },
    {
      label: "Cartão de débito",
      value: 3,
    },
    {
      label: "PIX",
      value: 4,
    },
  ];

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalFinishOrderVisible}
        onRequestClose={() =>
          setModalFinishOrderVisible(!modalFinishOrderVisible)
        }
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 20,
            paddingVertical: 10,
          }}
        >
          <Text style={{ fontSize: 20, color: "#6B7280" }}>
            Finalizar pedido
          </Text>
          <TouchableHighlight
            onPress={() => setModalFinishOrderVisible(!modalFinishOrderVisible)}
          >
            <Ionicons size={40} style={{ color: "#6B7280" }} name="close" />
          </TouchableHighlight>
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: "white", padding: 10 }}>
          <Text style={{ fontSize: 16, color: "#374151", fontWeight: "bold" }}>
            Forma de pagamento:
          </Text>
          <Text style={{ fontSize: 12, color: "#4B5563" }}>
            O pagamento deverá ser realizado no momento da entrega
          </Text>
          <RadioButtonRN
            data={paymentMethod}
            selectedBtn={(e: any) => console.log(e.value)}
          />
          <View style={{ marginTop: 20 }}>
            <Text
              style={{ fontSize: 16, color: "#374151", fontWeight: "bold" }}
            >
              Endereço de entrega:
            </Text>
            <Text style={{ fontSize: 12, color: "#4B5563" }}>
              Aonde devemos entregar seu pedido?
            </Text>
            <TextInput
              onChangeText={(text) => setOrderAddress(text)}
              value={orderAddress}
              placeholder={"EX: Rua das flores, 1300, centro"}
              style={{
                borderWidth: 1,
                borderRadius: 5,
                padding: 10,
                marginTop: 10,
                borderColor: "#D1D5DB",
                width: "100%",
              }}
            />
          </View>
        </ScrollView>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 20,
            paddingVertical: 10,
          }}
        >
          <TouchableHighlight
            style={{ flex: 1 }}
            onPress={() => setModalFinishOrderVisible(false)}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: 10,
                paddingHorizontal: 15,
                backgroundColor: "#D97706",
                borderRadius: 5,
              }}
            >
              <Text style={{ color: "#F9FAFB", fontWeight: "bold" }}>
                Finalizar pedido
              </Text>
            </View>
          </TouchableHighlight>
        </View>
      </Modal>
      {carrinho.length === 0 && (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <LottieView
            style={{
              width: "100%",
              height: 400,
            }}
            source={require("../assets/images/animation-empty-cart.json")}
            loop
            autoPlay
          />

          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text
              style={{
                textAlign: "center",
                textTransform: "uppercase",
                fontSize: 22,
                fontWeight: "bold",
                color: "#6B7280",
                paddingHorizontal: 30,
              }}
            >
              Seu carrinho de compras está vazio
            </Text>
            <Text
              style={{ textAlign: "center", fontSize: 18, color: "#6B7280" }}
            >
              Ver pratos
            </Text>
          </TouchableOpacity>
        </View>
      )}
      {carrinho.length !== 0 && (
        <Fragment>
          <ScrollView
            contentContainerStyle={{
              paddingHorizontal: 25,
            }}
          >
            <Text
              style={{
                fontSize: 20,
                fontWeight: "bold",
                color: "#374151",
                paddingVertical: 5,
              }}
            >
              Meus produtos:
            </Text>
            {carrinho?.map((produto: any) => (
              <View
                key={produto.hash}
                style={{
                  paddingHorizontal: 20,
                  paddingVertical: 15,
                  marginVertical: 8,
                  borderRadius: 10,
                  elevation: 1,
                }}
              >
                <Text
                  style={{ fontSize: 20, fontWeight: "bold", color: "#1F2937" }}
                >
                  {produto.name}
                </Text>
                <NumberFormat
                  style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: "#334155",
                  }}
                  value={produto.price}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    paddingVertical: 10,
                    marginTop: 10,
                  }}
                >
                  <TouchableOpacity onPress={() => subQtd(produto.hash)}>
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        paddingVertical: 10,
                        paddingHorizontal: 15,
                        backgroundColor: "#D1D5DB",
                        borderRadius: 5,
                      }}
                    >
                      <Ionicons
                        size={20}
                        style={{ color: "#374151" }}
                        name="remove"
                      />
                    </View>
                  </TouchableOpacity>
                  <Text
                    style={{
                      paddingHorizontal: 15,
                      paddingVertical: 5,
                      fontSize: 20,
                    }}
                  >
                    {produto.qtd}
                  </Text>
                  <TouchableOpacity onPress={() => addQtd(produto.hash)}>
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        paddingVertical: 10,
                        paddingHorizontal: 15,
                        backgroundColor: "#D1D5DB",
                        borderRadius: 5,
                      }}
                    >
                      <Ionicons
                        size={20}
                        style={{ color: "#374151" }}
                        name="add"
                      />
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ))}
            <View style={{ marginTop: 15 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: "bold",
                  color: "#374151",
                  paddingVertical: 5,
                }}
              >
                Resumo do pedido:
              </Text>
              <View
                style={{
                  backgroundColor: "#E5E7EB",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                }}
              >
                <Text style={{ fontWeight: "bold", color: "#374151" }}>
                  Subtotal
                </Text>
                <NumberFormat
                  value={total}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
              <View
                style={{
                  backgroundColor: "#fff",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                }}
              >
                <Text style={{ fontWeight: "bold", color: "#374151" }}>
                  Taxa de entrega
                </Text>
                <NumberFormat
                  value={deliveryFee}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
              <View
                style={{
                  backgroundColor: "#E5E7EB",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                }}
              >
                <Text style={{ fontWeight: "bold", color: "#374151" }}>
                  Total do pedido
                </Text>
                <NumberFormat
                  value={parseFloat((total + deliveryFee).toFixed(2))}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
            </View>
          </ScrollView>
          <View
            style={{
              flexDirection: "row",
              paddingHorizontal: 20,
              paddingVertical: 10,
            }}
          >
            <TouchableOpacity
              style={{ marginRight: 10 }}
              onPress={() => esvaziarCart()}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  backgroundColor: "#D1D5DB",
                  borderRadius: 5,
                }}
              >
                <Text style={{ color: "#374151" }}>Limpar</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={{ flex: 1 }}
              onPress={() =>
                setModalFinishOrderVisible(!modalFinishOrderVisible)
              }
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  backgroundColor: "#D97706",
                  borderRadius: 5,
                }}
              >
                <Text style={{ color: "#F9FAFB", fontWeight: "bold" }}>
                  Endereço de entrega
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </Fragment>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
});
