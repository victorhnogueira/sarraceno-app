import React, { useState, useEffect } from "react";
import {
  Image,
  StyleSheet,
  ScrollView,
  ToastAndroid,
  Modal,
  TouchableHighlight,
} from "react-native";
import NumberFormat from "react-number-format";

import { Text, View } from "../components/Themed";
import { TouchableOpacity, TextInput } from "react-native-gesture-handler";
import { Ionicons } from "@expo/vector-icons";
import { addProductToCart } from "../services/cart";

const pratos = [
  {
    id: 1,
    name: "Macarrão à Bolonhesa",
    description:
      "Macarrão espaguete à Bolonhesa com massa preparada a mão e especiarias da casa",
    image:
      "https://pizzariameurancho.com.br/wp-content/uploads/2016/06/espaguete-bolonhesa.jpg",
    price: 27.99,
    promotionalPrice: 25.99,
    promotionEnabled: true,
  },
  {
    id: 2,
    name: "Bistecca alla fiorentina",
    description: "Bistecca alla fiorentina carnes nobres e especiarias da casa",
    image:
      "https://media.gazetadopovo.com.br/vozes/2016/05/4-bisteca-alla-fiorentina-4-4037dec6.jpg",
    price: 60.0,
    promotionalPrice: 49.99,
    promotionEnabled: true,
  },
  {
    id: 4,
    name: "Vinho tinto brasileiro suave",
    description: "Garrafa de vinho tinto brasileiro suave",
    image:
      "https://cdnconfianca.loja.biz/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/v/i/vinho-tinto-suave-dom-bosco-750ml.jpg",
    price: 19.99,
    promotionalPrice: 14.99,
    promotionEnabled: false,
  },
  {
    id: 3,
    name: "Vinho Italiano Monte Tondo Amarone Della Valpolicella",
    description:
      "Garrafa de vinho Italiano Monte Tondo Amarone Della Valpolicella",
    image:
      "https://www.winemania.com.br/wp-content/uploads/2019/04/amarone.jpg",
    price: 600.0,
    promotionalPrice: 0,
    promotionEnabled: false,
  },
];

export default function TabOneScreen() {
  const [activeProduct, setActiveProduct] = useState<any>(null);
  const [activeProductQtd, setActiveProductQtd] = useState(1);
  const [activeProductObs, setActiveProductObs] = useState('')

  async function addProdutc(prato: any) {
    addProductToCart({
      id: prato.id,
      name: prato.name,
      obs: activeProductObs,
      price: prato.price,
      qtd: activeProductQtd,
    });
    ToastAndroid.showWithGravityAndOffset(
      "Produto adicionado ao carrinho!",
      ToastAndroid.LONG,
      ToastAndroid.BOTTOM,
      0,
      100
    );
    setActiveProduct(null);
  }

  useEffect(() => {
    if (!activeProduct) {
      setActiveProductQtd(1);
      setActiveProductObs('');
    }
  }, [activeProduct]);

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={!!activeProduct}
        onRequestClose={() => setActiveProduct(null)}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 20,
            paddingVertical: 10,
          }}
        >
          <Text style={{ fontSize: 20, color: "#6B7280" }}>
            Detalhes do produto
          </Text>
          <TouchableHighlight onPress={() => setActiveProduct(null)}>
            <Ionicons size={40} style={{ color: "#6B7280" }} name="close" />
          </TouchableHighlight>
        </View>
        <ScrollView style={{ flex: 1, backgroundColor: 'white' }}>
          <View style={styles.modalView}>
            {!!activeProduct?.image && (
              <View style={styles.itemImage}>
                <Image
                  style={styles.itemImageStretch}
                  source={{ uri: activeProduct?.image }}
                />
              </View>
            )}
            <View
              style={{
                marginTop: 5,
                paddingHorizontal: 10,
                width: "100%",
              }}
            >
              <Text
                style={{ fontSize: 24, fontWeight: "bold", color: "#334155" }}
              >
                {activeProduct?.name}
              </Text>
            </View>
            {activeProduct?.promotionEnabled && (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: 10,
                  width: "100%",
                }}
              >
                <NumberFormat
                  style={{
                    fontSize: 22,
                    fontWeight: "bold",
                    color: "#16A34A",
                    marginRight: 10,
                  }}
                  value={activeProduct?.price}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
                <NumberFormat
                  style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: "#334155",
                    textDecorationLine: "line-through",
                    textDecorationStyle: "solid",
                  }}
                  value={activeProduct?.promotionalPrice}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
            )}
            {!activeProduct?.promotionEnabled && (
              <View style={{ paddingHorizontal: 10, width: "100%" }}>
                <NumberFormat
                  style={{
                    fontSize: 22,
                    fontWeight: "bold",
                    color: "#16A34A",
                    marginRight: 10,
                  }}
                  value={activeProduct?.price}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
            )}

            <View
              style={{
                paddingVertical: 10,
                paddingHorizontal: 10,
              }}
            >
              <Text
                style={{ fontSize: 14, color: "#334155", textAlign: "justify" }}
              >
                {activeProduct?.description}
              </Text>
            </View>
            <Text
                style={{ fontSize: 12, fontWeight: 'bold', color: "#334155", marginTop: 20, width: '100%' }}
              >
                Alguma observação?
              </Text>
            <TextInput
              multiline
              numberOfLines={4}
              onChangeText={text => setActiveProductObs(text)}
              value={activeProductObs}
              placeholder={'EX: tirar cebola'}
              style={{ borderWidth: 1, borderRadius: 5, padding: 10, borderColor: '#D1D5DB', width: '100%', textAlignVertical: 'top' }}
              textBreakStrategy="simple"
            />
          </View>
        </ScrollView>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            paddingHorizontal: 20,
            paddingVertical: 10,
          }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              paddingVertical: 10,
              marginRight: 10,
            }}
          >
            <TouchableHighlight
              onPress={() => setActiveProductQtd(activeProductQtd - 1)}
              disabled={activeProductQtd <= 1}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  backgroundColor: "#D1D5DB",
                  borderRadius: 5,
                }}
              >
                <Ionicons
                  size={20}
                  style={{ color: "#374151" }}
                  name="remove"
                />
              </View>
            </TouchableHighlight>
            <Text
              style={{
                paddingHorizontal: 15,
                paddingVertical: 5,
                fontSize: 20,
              }}
            >
              {activeProductQtd}
            </Text>
            <TouchableHighlight
              onPress={() => setActiveProductQtd(activeProductQtd + 1)}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 10,
                  paddingHorizontal: 15,
                  backgroundColor: "#D1D5DB",
                  borderRadius: 5,
                }}
              >
                <Ionicons size={20} style={{ color: "#374151" }} name="add" />
              </View>
            </TouchableHighlight>
          </View>
          <TouchableHighlight
            style={{ flex: 1 }}
            onPress={() => addProdutc(activeProduct)}
          >
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                paddingVertical: 10,
                paddingHorizontal: 15,
                backgroundColor: "#D97706",
                borderRadius: 5,
              }}
            >
              <Text style={{ color: "#F9FAFB", fontWeight: "bold" }}>
                Adicionar ao carrinho
              </Text>
            </View>
          </TouchableHighlight>
        </View>
      </Modal>
      <ScrollView
        style={{ width: "100%" }}
        contentContainerStyle={{
          backgroundColor: "#F9FAFB",
          paddingHorizontal: 25,
        }}
      >
        {pratos?.map((prato) => (
          <View
            key={prato.id}
            style={{
              width: "100%",
              marginVertical: 10,
              borderRadius: 5,
              backgroundColor: "#F3F4F6",
              shadowColor: "black",
              shadowRadius: 5,
              elevation: 3,
            }}
          >
            {!!prato.image && (
              <View style={styles.itemImage}>
                <Image
                  style={styles.itemImageStretch}
                  source={{ uri: prato.image }}
                />
              </View>
            )}
            <View
              style={{
                marginTop: 5,
                paddingHorizontal: 10,
                backgroundColor: "#F3F4F6",
              }}
            >
              <Text
                style={{ fontSize: 24, fontWeight: "bold", color: "#334155" }}
              >
                {prato.name}
              </Text>
            </View>
            {prato.promotionEnabled && (
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  paddingHorizontal: 10,
                  backgroundColor: "#F3F4F6",
                }}
              >
                <NumberFormat
                  style={{
                    fontSize: 22,
                    fontWeight: "bold",
                    color: "#16A34A",
                    marginRight: 10,
                  }}
                  value={prato.price}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
                <NumberFormat
                  style={{
                    fontSize: 20,
                    fontWeight: "bold",
                    color: "#334155",
                    textDecorationLine: "line-through",
                    textDecorationStyle: "solid",
                  }}
                  value={prato.promotionalPrice}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
            )}
            {!prato.promotionEnabled && (
              <View
                style={{ paddingHorizontal: 10, backgroundColor: "#F3F4F6" }}
              >
                <NumberFormat
                  style={{
                    fontSize: 22,
                    fontWeight: "bold",
                    color: "#16A34A",
                    marginRight: 10,
                  }}
                  value={prato.price}
                  displayType={"text"}
                  decimalSeparator={","}
                  prefix={"R$"}
                  // @ts-ignore
                  renderText={(value, props) => <Text {...props}>{value}</Text>}
                />
              </View>
            )}

            <View
              style={{
                paddingVertical: 10,
                paddingHorizontal: 10,
                backgroundColor: "#F3F4F6",
              }}
            >
              <Text
                style={{ fontSize: 14, color: "#334155", textAlign: "justify" }}
              >
                {prato.description}
              </Text>
            </View>
            <View style={{ flexDirection: "row", backgroundColor: "#F3F4F6" }}>
              <TouchableOpacity onPress={() => console.log("ola")}>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginVertical: 10,
                    marginHorizontal: 10,
                    paddingVertical: 10,
                    paddingHorizontal: 20,
                    backgroundColor: "#FBBF24",
                    borderRadius: 5,
                  }}
                >
                  <Ionicons size={20} style={{ color: "white" }} name="star" />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => setActiveProduct(prato)}>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    marginVertical: 10,
                    paddingVertical: 10,
                    paddingHorizontal: 20,
                    backgroundColor: "#374151",
                    borderRadius: 5,
                  }}
                >
                  <Text style={{ fontSize: 14, color: "#F3F4F6" }}>
                    ADICIONAR AO CARRINHO
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        ))}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
    fontWeight: "bold",
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: "80%",
  },
  itemImage: {
    width: "100%",
  },
  itemImageStretch: {
    width: "100%",
    height: 200,
  },
  modalView: {
    flex: 1,
    padding: 20,
    backgroundColor: "white",
    alignItems: "center",
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
});
