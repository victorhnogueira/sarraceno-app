import AsyncStorage from "@react-native-async-storage/async-storage";

interface Product {
  id: number;
  hash: string;
  name: string;
  price: number;
  qtd: number;
  obs: string;
}

export const cartKey = "@sarraceno-cart";

export const getCart = async () => await AsyncStorage.getItem(cartKey);

export const addProductToCart = async (product: Omit<Product, "hash">) => {
  const shoppingCart: Product[] = JSON.parse((await getCart()) ?? "[]");
  const hash = Math.random().toString(36).substring(7);

  let newCart: Product[] = [];

  if (shoppingCart.length === 0) {
    newCart = [{ ...product, hash }];
  } else {
    newCart = [...shoppingCart, { ...product, hash }];
  }

  await AsyncStorage.setItem(cartKey, JSON.stringify(newCart));
};

export const subProductQtd = async (hash: string): Promise<void> => {
  const shoppingCart: Product[] = JSON.parse((await getCart()) ?? "[]");

  const product = shoppingCart.find((product) => product.hash === hash);

  if (product?.qtd === 1) {
    await removeProductFromCart(hash);
  } else {
    const newCart = shoppingCart.map((product) => {
      if (product.hash === hash) {
        return {
          ...product,
          qtd: product.qtd - 1,
        };
      }

      return product;
    });

    await AsyncStorage.setItem(cartKey, JSON.stringify(newCart));
  }
};

export const addProductQtd = async (hash: string): Promise<void> => {
  const shoppingCart: Product[] = JSON.parse((await getCart()) ?? "[]");

  const newCart = shoppingCart.map((product) => {
    if (product.hash === hash) {
      return {
        ...product,
        qtd: product.qtd + 1,
      };
    }

    return product;
  });

  await AsyncStorage.setItem(cartKey, JSON.stringify(newCart));
};

export const removeProductFromCart = async (hash: string): Promise<void> => {
  const shoppingCart: Product[] = JSON.parse((await getCart()) ?? "[]");

  const newCart = shoppingCart.filter((product) => product.hash !== hash);

  await AsyncStorage.setItem(cartKey, JSON.stringify(newCart));
};


export const getTotal = async (): Promise<number> => {
  const shoppingCart: Product[] = JSON.parse((await getCart()) ?? "[]");

  const total = shoppingCart.reduce((acc, product) => acc + (product.qtd * product.price), 0).toFixed(2)

  return parseFloat(total)
};

export const emptyCart = async () => {
  await AsyncStorage.removeItem(cartKey);
};
