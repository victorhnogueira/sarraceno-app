/**
 * Learn more about createBottomTabNavigator:
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */

import { Ionicons } from "@expo/vector-icons";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";
import * as React from "react";

import Colors from "../constants/Colors";
import useColorScheme from "../hooks/useColorScheme";
import TabPratosScreen from "../screens/TabPratosScreen";
import TabShoppingCartScreen from "../screens/TabShoppingCartScreen";
import {
  BottomTabParamList,
  TabPratosParamList,
  TabShoppingCartParamList,
} from "../types";

const BottomTab = createBottomTabNavigator<BottomTabParamList>();

export default function BottomTabNavigator() {
  const colorScheme = useColorScheme();

  return (
    <BottomTab.Navigator
      initialRouteName="TabPratos"
      tabBarOptions={{ activeTintColor: Colors[colorScheme].tint }}
    >
      <BottomTab.Screen
        name="TabPratos"
        component={TabPratosNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="home-outline" color={color} />
          ),
          tabBarLabel: "Pratos",
        }}
      />
      <BottomTab.Screen
        name="TabShoppingCart"
        component={TabShoppingCartNavigator}
        options={{
          tabBarIcon: ({ color }) => (
            <TabBarIcon name="cart-outline" color={color} />
          ),
          tabBarLabel: "Carrinho"
        }}
      />
    </BottomTab.Navigator>
  );
}

// You can explore the built-in icon families and icons on the web at:
// https://icons.expo.fyi/
function TabBarIcon(props: {
  name: React.ComponentProps<typeof Ionicons>["name"];
  color: string;
}) {
  return <Ionicons size={30} style={{ marginBottom: -3 }} {...props} />;
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const TabPratosStack = createStackNavigator<TabPratosParamList>();

function TabPratosNavigator() {
  return (
    <TabPratosStack.Navigator>
      <TabPratosStack.Screen
        name="TabPratosScreen"
        component={TabPratosScreen}
        options={{ headerTitle: "Pratos" }}
      />
    </TabPratosStack.Navigator>
  );
}

const TabShoppingCartStack = createStackNavigator<TabShoppingCartParamList>();

function TabShoppingCartNavigator() {
  return (
    <TabShoppingCartStack.Navigator>
      <TabShoppingCartStack.Screen
        name="TabShoppingCartScreen"
        component={TabShoppingCartScreen}
        options={{ headerTitle: "Carrinho de compras" }}
      />
    </TabShoppingCartStack.Navigator>
  );
}
